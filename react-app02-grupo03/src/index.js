import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  return (
      <button
          className={ props.winner ? 'square2' : 'square' }
          onClick={props.onClick}
      >
          {props.value}
      </button>
  );
}

class Board extends React.Component {

  isWinnerSquare(i) {
      if(this.props.winner && this.props.winner.line.findIndex(el => el === i) !== -1) {
          return true;
      }
      return null;
  }

  renderSquare(i) {
      return (
          <Square
              value={this.props.squares[i]}
              onClick={() => this.props.onClick(i)}
              winner={this.isWinnerSquare(i)}
              key={i}
          />
      );
  }

  render() {
      return (
          <div className="board">
              <div className="row-counters">
                  
              </div>
              {
                  Array(3).fill(null).map((row, x) => {
                      return (
                          <div className="board-row" key={x} >
                              {
                                  Array(3).fill(null).map((square, y) => this.renderSquare(3 * x + y))
                              }
                          </div>
                      )
                  })
              }
              <div className="column-counters">
                  
              </div>
          </div>
      );
  }
}

class Game extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          history: [
              {
                  squares: Array(9).fill(null),
                  position: null,
              }
          ],
          XIsNext: true,
          stepNumber: 0,
      }
  }

  handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();

      const position = getPosition(i);

      if (caculateWinner(squares) || squares[i]) {
          return;
      }

      squares[i] = this.state.XIsNext ? 'X' : 'O';
      this.setState({ 
          history: history.concat([{
              squares,
              position,
          }]),
          stepNumber: history.length,
          XIsNext: !this.state.XIsNext,
          
      });
  }

  jumpTo(step) {
      this.setState({
          stepNumber: step,
          XIsNext: (step % 2) === 0,
      });
  }

 

  render() {
      const history = this.state.history;
      const current = history[this.state.stepNumber];
      const winner = caculateWinner(current.squares);

      const moves = history.map((step, move) => {
          const desc = move ? 
              'Go to move #' + move + ` ( ${history[move].position.join(',')} )` :
              'Go to game start';
          return (
              <li key={move} >
                  <button 
                      onClick={() => this.jumpTo(move)}
                  >{desc}</button>
              </li>
          );
      })

      
      let status;
      if (winner) {
          status = 'Ganador: ' + winner.name;
      } else {
        status = `Siguiente jugador: ${ this.state.XIsNext ? 'X' : 'O'}`;
      }

      return (
          <div className="game">
              <div className="game-board">
                  <Board 
                      squares = {current.squares}
                      winner = {winner}
                      onClick = {(i) => this.handleClick(i)}
                  />
              </div>
              <div className="game-info">
                  <div>{status}</div>
                  
                  <ol>{moves}</ol>
              </div>
          </div>
      );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function caculateWinner(squares) {
  const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
          return {
              name: squares[a],
              line: lines[i],
          };
      }
  }
  return null;
}

function getPosition(i) {
  const rowMap = [
      [1,1], [2, 1], [3, 1],
      [1,2], [2, 2], [3, 2],
      [1,3], [2, 3], [3, 3],
  ]
  return rowMap[i];
}