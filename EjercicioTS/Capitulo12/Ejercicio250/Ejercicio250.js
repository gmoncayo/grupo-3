var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
function showSpread(one, two, three, four) {
    console.log(one);
    console.log(two);
    console.log(three);
    console.log(four);
}
var myArray = [1, 2, 3, 4];
//showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread
//showSpread(...myArray); //using the spread syntax
document.write("spread Syntax en fechas: ");
var dayInfo = [1975, 7, 19];
var dateObj = new Date("1975-07-19");
console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
document.write("<br>" + dateObj);
var numArray2 = [2, 3, 4, 5, 6, 7];
var numArray = __spreadArrays([1], numArray2, [8, 9, 10]);
document.write("<br>" + "spread Syntax  en array:");
document.write("<br>" + numArray);
console.log(numArray); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
var part1 = [1, 2, 3];
var part2 = [4, 5, 6];
var part3 = __spreadArrays(part1, part2);
document.write("<br>" + "spread Syntax  en array 2:");
document.write("<br>" + part3);
console.log(part3); //returns 1,2,3,4,5,6
