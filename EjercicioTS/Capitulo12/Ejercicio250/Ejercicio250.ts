function showSpread(one, two, three, four){ 
    console.log(one);  
    console.log(two);   
    console.log(three);
    console.log(four);
}

var myArray: number[] = [1,2,3,4];

//showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread

//showSpread(...myArray); //using the spread syntax

document.write("spread Syntax en fechas: ") 
var dayInfo : number[] = [1975, 7, 19];
var dateObj = new Date("1975-07-19");
console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
document.write("<br>" +dateObj);

var numArray2:  number[] = [ 2, 3, 4, 5, 6, 7]
var numArray:  number[] = [1, ...numArray2, 8, 9, 10];

document.write("<br>" +"spread Syntax  en array:");
document.write("<br>" +numArray)

console.log(numArray); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

var part1: number[] = [1,2,3];
var part2: number[] = [4,5,6];

var part3: number[] = [...part1, ...part2];

document.write("<br>" +"spread Syntax  en array 2:");
document.write("<br>" +part3)


console.log(part3); //returns 1,2,3,4,5,6