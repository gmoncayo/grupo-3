var _this = this;
//ES 6
document.getElementById('myButton').addEventListener('click', function () {
    _this.currentInterval = 0;
    setInterval(function () { _this.currentInterval++; }, 1000);
}); //retuning object literals
var myObj = function () { return ({ name: 'June' }); };
console.log(myObj()); //returns Object {name: "June"}
