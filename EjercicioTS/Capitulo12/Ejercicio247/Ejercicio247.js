console.log(this);
function globalFunction(){
    return this;
}

console.log(globalFunction());
console.log(window.globalFunction());
function globalStrictFunction(){
    'use strict'
return this;
}
console.log(globalStrictFunction());
console.log(window.globalStrictFunction());
function saySomething(){
    return this.something;
}
var phrase = saySomething.bind({something: 'Brothers! Sisters!'});
console.log(saySomething());
console.log(phrase());
function useCallFunction(){
    return this.greeting;
}
var greetingObj = {greeting: 'Hello, Mr. Robot'};

console.log(useCallFunction.call(greetingObj));
console.log(useCallFunction.apply(greetingObj));
document.getElementById('myButton').addEventListener('click', function(e){
    console.log(this);
});
var globalArrayFunction = () => this;
console.log(globalArrayFunction());
var miCheck = {
    inThisOn: function(){
        return(()=> this);
    }
}
var returnedFuction = micCheck.inThisOn();
console.log(returnedFuction());
var theNumber = {p:42};
    function magicNumber(){
        return this.p;
    }

theNumber.theMagicNumber = magicNumber;
console.log(theNumber.theMagicNumber());
