/* Comprobando si un valor no es un número


Problema
Recibe datos y necesita verificar si el valor es un número.*/

var numberObject = new Number('things');
function checkIsNaN (value: string){
if(typeof value !== 'number'){
return 'is not a number';
}else{
return 'is a number';
}
}

console.log(checkIsNaN('1234'));//returns is not a number
console.log(isNaN('things') );// returns true
console.log(Number.isNaN(numberObject.valueOf()));//returns true
console.log(Number.isNaN(4)); //returns false
console.log(Number.isNaN(NaN)); //returns true