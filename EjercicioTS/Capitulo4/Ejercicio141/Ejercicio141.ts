var myNumber:number = 1;

console.log(Math.floor(1.6));
console.log(Math.floor(1.4));
console.log(Math.floor(NaN));

console.log(Math.ceil(1.6));
console.log(Math.ceil(1.4));

console.log(Math.round(1.6));
console.log(Math.round(1.4));

console.log(Math.round(-1.5));
console.log(Math.round(-1.6));

console.log(Math.round(null));