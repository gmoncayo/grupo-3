function multi_array_1_1() {
    var multiDArray = [[7, 8], [3, 4], [1, 2], [5, 6]];
    multiDArray.sort(function (a, b) {
        return a[0] - b[0];
    });
    console.log(multiDArray); //returns [[1,2], [3,4], [5,6], [7,8]];
    alert(multiDArray);
}
function element_array_1_1() {
    var myArray = [9, 2, 7, 6, 8, 5, 3];
    myArray.forEach(function (element, index, array) {
        console.log(element + ' element'); //returns 9, 2, 7, 6, 8, 5, 3     
        console.log(element + ' index '); //returns 0 ,1, 2, 3, 4, 5, 6     
        console.log(element + ' array '); //returns the entire array
        var vector_result = [element + ' element', element + ' index ',
            element + ' array '];
        alert(vector_result);
    });
}
