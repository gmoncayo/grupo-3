"use strict";
/*¿Cuál es la diferencia entre un constructor de objetos y un objeto literal?

Problema
Desea saber cuál es la diferencia entre crear un objeto utilizando el nuevo operador o un objeto
literal.*/
var R2D2 = new Object();
R2D2.class = 'Astromech Droid';
var R2D2 = {
    class: 'Astromech Droid',
    manufacturer: 'Industrial Automaton'
};
console.log(R2D2.class);
console.log(R2D2.manufacturer);
