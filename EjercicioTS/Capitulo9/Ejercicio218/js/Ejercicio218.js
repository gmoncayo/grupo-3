"use strict";
function optimus() {
    var optimusPrime = Object.freeze("robot1");
    console.log(Object.isFrozen(optimusPrime)); //returns truevar 
    var hotRod = Object.seal("robot2");
    console.log(Object.isSealed(hotRod)); //returns truevar 
    var jazz = Object.preventExtensions("robot3");
    console.log(Object.isExtensible(jazz)); //returns false
    var text1 = "isfrozen" + Object.isFrozen(optimusPrime);
    var text2 = "issealed" + Object.isSealed(hotRod);
    var text3 = "isextensible" + Object.isExtensible(jazz);
    var result = [text1, text2, text3];
}
