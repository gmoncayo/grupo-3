function optimus(): void{

  var optimusPrime: Object = Object.freeze("robot1");    
  console.log(Object.isFrozen(optimusPrime));  //returns truevar 
  var hotRod = Object.seal("robot2");    
  console.log(Object.isSealed(hotRod)); //returns truevar 
  var jazz = Object.preventExtensions("robot3");    
  console.log(Object.isExtensible(jazz)); //returns false
  var text1: string="isfrozen" + Object.isFrozen(optimusPrime);
  var text2: string="issealed" + Object.isSealed(hotRod);
  var text3: string="isextensible" + Object.isExtensible(jazz);
  let result=[text1,text2,text3];

}