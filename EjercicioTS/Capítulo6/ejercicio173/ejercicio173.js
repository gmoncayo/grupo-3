"use strict";
var now = new Date(); //retorna el dia y hora del dia actual<br>
console.log(now);
var fluxCapacitorDate = new Date('November 5, 1955');
console.log(fluxCapacitorDate);
var infinityWarDate = new Date(2018, 4, 4); //retorna la fecha a la media noche<br>
console.log(infinityWarDate);
var unixTimestamp = Date.now(); // retorna la fecha actual representada en milisegundos<br>
console.log(unixTimestamp);
