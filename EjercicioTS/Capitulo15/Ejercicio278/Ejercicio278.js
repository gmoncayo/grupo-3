function TrapMessage() {
    return "It's a Trap!!!";
}
var handler = {
    apply: function (target, thisArg) {
        return target.apply(thisArg);
    }
};
var proxy = new proxy(TrapMessage, handler);
console.log(proxy());
