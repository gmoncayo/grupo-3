function TrapMessage () {
    return "It's a Trap!!!";
}

var handler = {
apply: function(target: any , thisArg: any):any {
     return target.apply(thisArg);    
}
};
var proxy : any = new proxy(TrapMessage, handler);
console.log(proxy()); 