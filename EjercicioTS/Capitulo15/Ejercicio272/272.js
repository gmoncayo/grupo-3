var CheckArrayInstance_1 = /** @class */ (function () {
    function CheckArrayInstance() {
    }
    CheckArrayInstance[Symbol.hasInstance] = function (instance) { return Array.isArray(instance); };
    return CheckArrayInstance;
}());
var myArray = new Array();
console.log(myArray instanceof CheckArrayInstance); //returns true
function print_console() {
    alert(myArray instanceof CheckArrayInstance);
}
