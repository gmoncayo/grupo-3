//ECMAScript 5 class
var Human = (function Human(name) {
    this.name = name;
});
Human.prototype.sayGoodNight = function () {
    return 'Say Goodnight ' + this.name;
};
var george = new Human('Gracie');
console.log(george.sayGoodNight());
//ECMAScript 6 class
var Greeting = /** @class */ (function () {
    function Greeting(name) {
        this.greeting = name;
    }
    Greeting.prototype.sayHello = function () {
        return 'Hellooo ' + this.greeting;
    };
    return Greeting;
}());
var yakko = new Greeting('Nurse!');
console.log(yakko.sayHello());
document.write(yakko.sayHello());
