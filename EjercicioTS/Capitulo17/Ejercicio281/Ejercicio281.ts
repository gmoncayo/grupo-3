//ECMAScript 5 class

var Human = (
    function Human(name){ 
                        this.name = name;        
    }
    
)

Human.prototype.sayGoodNight = function(){
   return 'Say Goodnight ' + this.name;

}

var george = new Human('Gracie');
console.log(george.sayGoodNight());

//ECMAScript 6 class

class Greeting{
    greeting: string;
     constructor(name: string){
              this.greeting = name;
       }

 sayHello(){ 
         return 'Hellooo ' +  this.greeting;
}
}

var yakko = new Greeting('Nurse!');
console.log(yakko.sayHello());

document.write(yakko.sayHello());



