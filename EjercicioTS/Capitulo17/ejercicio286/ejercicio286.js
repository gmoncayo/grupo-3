"use strict";
var Human = /** @class */ (function () {
    function Human() {
    }
    Human.hasLegs = function () {
        return 'Person has legs';
    };
    Human.hasAmrs = function () {
        return 'Person has arms';
    };
    return Human;
}());
