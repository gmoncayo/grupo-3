"use strict";
/*¿Cuáles son las ventajas de usar un mapa sobre un objeto?

Problema
Si bien existen similitudes con los mapas y los objetos, los mapas pueden contener objetos y primitivas como claves o valores..*/
var mapObj = new Map();
mapObj.set('myString', 'myString is a string key');
console.log(mapObj.get('myString')); //myString is a string key
var myObj = {};
mapObj.set(myObj, 'Object is a used as a key');
console.log(mapObj.get(myObj)); //Object is a used as a key
