var a = 'this is a string'; // A String literal (using Single Quotes)
var b = "this is another string"; // A String literal (using Double Quotes)
var c = true; // A Boolean literal
var d = false; // A Boolean literal
var e = 3; // A Number literal
var f = 3.1415926; // Another Number literal
5; // A Binary Number literal
0xFFF; // A Hexadecimal Number literal
[]; // An (empty) Array literal
var g = [1, 2, 3,]; // An Array literal populated with 3 Number literals
var h = ['hello', 'world']; // An Array literal populated with 2 String literals{}; // An (empty) Object literal
{ }
; // An (empty) Object literal
// An Object literal populated with 2 Number literals, named first
/abc/; // A Regular Expression literal
/^\w{3,4}$/g; // A more complex Regular Expression literal
function Name() { }
; // A Function literal
function Foo() { }
; // A named Function literal
null; // The null literal
undefined; // The undefined literal
"this is a template string"; // A Template String literal
"Hello #{name}\nHow are you?"; // A more complex Template String literal
console.log('this is a string');
