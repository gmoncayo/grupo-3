"use strict";
if (0) {
    console.log("0 is true");
    document.write("<br>" + '0 is true');
}
else {
    console.log("0 is false");
    document.write("<br>" + '0 is false');
}
if (-0) {
    console.log("-0 is true");
    document.write("<br>" + '-0 is true');
}
else {
    console.log("-0 is false");
    document.write("<br>" + '-0 is false');
}
if (null) {
    console.log("null is true");
    document.write("<br>" + 'null is true');
}
else {
    console.log("null is false");
    document.write("<br>" + 'null is false');
}
if (false) {
    console.log("false is true");
    document.write("<br>" + 'false is true');
}
else {
    console.log("false is false");
    document.write("<br>" + 'false is false');
}
if (NaN) {
    console.log("NaN is true");
    document.write("<br>" + 'NaN is true');
}
else {
    console.log("NaN is false");
    document.write("<br>" + 'NaN is false');
}
if (undefined) {
    console.log("undefined is true");
    document.write("<br>" + 'undefined is true');
}
else {
    console.log("undefined is false");
    document.write("<br>" + 'undefined is false');
}
if ("") {
    console.log("EMPTY is true");
    document.write("<br>" + 'empty is true');
}
else {
    console.log("EMPTY is false");
    document.write("<br>" + 'empty is false');
}
// Examples of conditional or comparative expressionsvar 
var c = 1 + 2;
var d = null;
var a = 0;
var e = a - 3;
var f = undefined;
if (c) {
    console.log("c is true");
    document.write("<br>" + 'c is true');
}
else {
    console.log("c is false");
    document.write("<br>" + 'c is false');
}
if (d) {
    console.log("d is true");
    document.write("<br>" + 'd is true');
}
else {
    console.log("d is false");
    document.write("<br>" + 'd is false');
}
if (e) {
    console.log("e is true");
    document.write("<br>" + 'e is true');
}
else {
    console.log("e is false");
    document.write("<br>" + 'e is false');
}
if (f) {
    console.log("f is true");
    document.write("<br>" + 'f is true');
}
else {
    console.log("f is false");
    document.write("<br>" + 'f is false');
}
