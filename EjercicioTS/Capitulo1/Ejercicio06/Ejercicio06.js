"use strict";
/* Asignación de variables con la instrucción Let
Problema
Al usar una variable, desea que su vida sea más corta que el alcance funcional. Por ejemplo, desea que las variables vida existan dentro de una declaración if.*/
var a = 4; // The variable "a" has been declared and assigned to `4`
var b = 2; // The variable "b" has been declared and assigned to `2`
{
    console.log(b); // Logs 2
}
if (a === 4) { // For more on the Strict Equality Operator (`===`), see Chapter 2-10
    var a_1 = 1; // The variable "a" has been redeclared in this block scope (the braces)
    console.log(a_1); // Logs 1
}
console.log(b); // Raises a ReferenceError in strict mode, or logs undefined
console.log(a); // Logs 4, the `let a = 1` in the block scope above was only associated with that block scope
