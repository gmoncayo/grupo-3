console.log(Boolean(-0));
console.log(Boolean(0));
console.log(Boolean(new Number(0)));
console.log(Boolean(1));
console.log(Boolean(NaN));
console.log(Boolean(-1));

console.log(Boolean(false));
console.log(Boolean(true));

console.log(Boolean(undefined));
console.log(Boolean(null));

console.log(Boolean(new String()));
console.log(Boolean(""));
console.log(Boolean('a string'));
console.log(Boolean("true"));
console.log(Boolean("false"));

console.log(Boolean(function(){}));
console.log(Boolean({}));
console.log(Boolean([]));