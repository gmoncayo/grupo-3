import React, { useState, useEffect } from "react";
import { db } from "../firebase";

const LinkForm = (props) => {
    
    /* Inicialización de variables de los campos (Manejo de estados, estados vacíos)*/
    const initialStateValues = {
        nombres: "",
        correo: "",
        nacimiento: "",
        edad: "",
        telefono: "",
    };

    /* Cálculo de la edad */
    function calculaEdad() {
        var hoy = new Date();
        var fecha = document.getElementById("IDnacimiento");
        var cumpleanos = new Date(fecha.value);
        var edad = hoy.getFullYear() - cumpleanos.getFullYear();
        var m = hoy.getMonth() - cumpleanos.getMonth();
    
        if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
            edad--;
        }
        values.edad=edad;
        console.log(values.edad);
    }

    const [values, setValues] = useState(initialStateValues);

    /* Manejo del campo de entrada de datos de input */
    /* Aquí están los datos en values */
    const handleInputChange = (e) => {
        const {name, value } = e.target;
        setValues({...values, [name]:value});
    }; 

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(values); //ver información del evento antes del cálculo de la edad
        console.log("calcula EDAD");
        calculaEdad(); // función que calcula la edad
        console.log(values); // ver información del evento después del cálculo de la edad
        props.addOrEditLink(values);
        setValues({...initialStateValues});
    };

    const getLinkById = async (id) => {
        const doc = await db.collection('links').doc(id).get();
        setValues({...doc.data()});
    }

    /* Validaciones */
    useEffect(() => { 
        if (props.currentId === "") {
            setValues({ ...initialStateValues });
        } else {
            getLinkById(props.currentId);
        }
    }, [props.currentId]);

    return (
     /* Formulario */   
     <form id="form1" className="was-validated" onSubmit={handleSubmit} noValidate="">
        <div className="form-row"> 
        {/* Campo de entrada de datos 1 */}
        <div className="col-md-16 mb-2 form-group input-group">
            {/* Icono 1 */}  
             <div className="input-group-text bg-light">
                <i className="material-icons">person</i>
             </div> 
             {/* Entrada 1 */} 
             <input 
             type="text"
             className="form-control"
             id="IDnombres"
             placeholder="Nombre Apellido"
             name="nombres"
             onChange={handleInputChange}
             value={values.nombres} required />
             <div className="valid-feedback">¡Ok válido!</div>
             <div className="invalid-feedback">* Campo requerido</div>
        </div>    
        {/* Campo de entrada de datos 2 */}
        <div className="form-group input-group">
             {/* Icono 2*/}  
             <div className="input-group-text bg-light">
              <i className="material-icons">email</i>  
             </div> 
             {/* Entrada 2 */} 
             <input type="email" className="form-control" id="IDcorreo" placeholder="name@example.com" name="correo" 
             onChange={handleInputChange}
             value={values.correo} required/>
             <div className="valid-feedback">¡Ok válido!</div>
             <div className="invalid-feedback">* Campo requerido</div>
        </div>

         {/* Campo de entrada de datos 2.5 */}
         <div className="form-group input-group">
             {/* Icono 2.5*/}  
             <div className="input-group-text bg-light">
              <i className="material-icons">phone</i>  
             </div> 
             {/* Entrada 2.5 */} 
             <input type="number" className="form-control" id="IDtelefono" placeholder="1234567" name="telefono" 
             onChange={handleInputChange}
             value={values.telefono} required/>
             <div className="valid-feedback">¡Ok válido!</div>
             <div className="invalid-feedback">* Campo requerido</div>
        </div>

        {/* Campo de entrada de datos 3 */}
        <div className="form-group input-group">
             {/* Icono 3*/}  
             <div className="input-group-text bg-light">
              <i className="material-icons">cake</i>  
             </div> 
             {/* Entrada 3 */} 
             <input 
             type="date"
             className="form-control"
             id="IDnacimiento"
             placeholder="Fecha de nacimiento"
             name="nacimiento" 
             onChange={handleInputChange}
             value={values.nacimiento}
             required
             />
             <div className="valid-feedback">¡Ok válido!</div>
             <div className="invalid-feedback">* Campo requerido</div>
        </div>

        {/* Botón para Grabar o Actualizar */}
        <button className="btn btn-primary btn-block">
            {props.currentId === '' ? 'Añadir': 'Actualizar'}
        </button>
     </div>        
     </form>

    )
}
export default LinkForm;