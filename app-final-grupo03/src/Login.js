import React from "react";

const Login = (props) => {

    const {
        email, 
        setEmail, 
        password, 
        setPassword, 
        handleLogin, 
        handleSigup, 
        hasAccount, 
        setHasAccount, 
        emailError, 
        passwordError,
    } = props;

    return(
        <section className="login">
            <div className="loginContainer">
                <label>Nombre de usuario</label>
                <input
                    type="text"
                    autoFocus
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <p className="errorMsg">{emailError}</p>
                <label>Contraseña</label>
                <input 
                    type="password" 
                    required 
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                />
                <p className="errorMsg">{passwordError}</p>
                <div className="btnContainer">
                    {hasAccount ? (
                        <>
                            <button onClick={handleLogin}>Iniciar sesión</button>
                            <p>
                                ¿ No tiene una cuenta ? 
                                <span onClick={() => setHasAccount(!hasAccount)}>Registrarse</span>
                            </p>
                        </>                            
                    ) : (
                        <>
                            <button onClick={handleSigup}>Registrarse</button>                        
                            <p>
                                ¿ Tiene una cuenta ? 
                                <span onClick={() => setHasAccount(!hasAccount)}>Iniciar sesión</span></p>
                        </>
                    ) }
                </div>
            </div>
        </section>
    );
    };

export default Login;