import firebase from 'firebase/app'  
import 'firebase/firestore'

  var firebaseConfig = {
    apiKey: "AIzaSyBL6FdRSnNowe_eNCBKlWdHQrVdidZ7Elw",
    authDomain: "app-final-grupo3.firebaseapp.com",
    databaseURL: "https://app-final-grupo3.firebaseio.com",
    projectId: "app-final-grupo3",
    storageBucket: "app-final-grupo3.appspot.com",
    messagingSenderId: "681806412649",
    appId: "1:681806412649:web:41b10936b5318360031bf8"
  };
  // Initialize Firebase
  const fb = firebase.initializeApp(firebaseConfig);
  export const db = fb.firestore();
  