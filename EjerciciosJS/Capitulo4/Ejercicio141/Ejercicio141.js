var myNumber = 1;
Number.isInteger(myNumber);
Number.isInteger('2');

Math.floor(1.6);
Math.floor(1.4);
Math.floor(NaN);

Math.ceil(1.6);
Math.ceil(1.4);

Math.round(1.6);
Math.round(1.4);

Math.round(-1.5);
Math.round(-1.6);

Math.round('3.5');
Math.round('jenny');
Math.round(null);
