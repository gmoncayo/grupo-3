a='this is a string'; // A String literal (using Single Quotes)
b="this is another string"; // A String literal (using Double Quotes)
c=true; // A Boolean literal
d=false; // A Boolean literal
e=3; // A Number literal
f=3.1415926; // Another Number literal
0b0101; // A Binary Number literal
0xFFF; // A Hexadecimal Number literal
[]; // An (empty) Array literal
g=[ 1, 2, 3, ]; // An Array literal populated with 3 Number literals
h=[ 'hello', 'world']; // An Array literal populated with 2 String literals{}; // An (empty) Object literal
{}; // An (empty) Object literal
{ first: 1,  2 }; // An Object literal populated with 2 Number literals, named first
/abc/; // A Regular Expression literal
/^\w{3,4}$/g; // A more complex Regular Expression literal
function name () {}; // A Function literal
function foo() {}; // A named Function literal
null; // The null literal
undefined; // The undefined literal
`this is a template string`; // A Template String literal
`Hello #{name}
How are you?`; // A more complex Template String literal

console.log('this is a string');

document.write(a);
document.write("<br>" +b);
document.write("<br>" +c);
document.write("<br>" +d);
document.write("<br>" +e);
document.write("<br>" +f);
document.write("<br>" +g);
document.write("<br>" +h);