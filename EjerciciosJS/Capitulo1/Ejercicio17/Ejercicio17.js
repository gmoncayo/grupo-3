/*  Realización de operaciones si una variable definida tiene un valor
Problema
Debe realizar diferentes operaciones en función del valor de una variable. Es posible que deba probar que una variable es estrictamente igual, simplemente equivalente o estrictamente no igual a un valor.*/

var myNumber = 10;
// Determine if myNumber is equivalent to the number 10
if ( myNumber === 10 ) { // For more on the Strict Equality Operator (`===`), see Chapter 2-10
console.log( "myNumber es igual a 10");
}
else {
console.log( "myNumber no es igual a 10");
}
// Determine if myNumber is less than the number 10
if ( myNumber < 10 ) {
console.log( "myNumber es menor que 10");
}
else {
console.log( "myNumber es mayor o igual que 10");
}
// Determine if myNumber is less than or equal to the number 10
if ( myNumber <= 10 ) {
console.log( "myNumber es menor or igual que 10");
}
else {
console.log( "myNumber es mayor que 10");
}
// Determine if myNumber is greater than the number 10
if ( myNumber > 10 ) {
  console.log( "myNumber es mayor 10");
  }
  else {
  console.log( "myNumber es menor o igual que 10");
  }
  // Determine if myNumber is greater than or equal to the number 10
  if ( myNumber >= 10 ) {
  console.log( "myNumber es mayor o igual que 10");
  }
  else {
  console.log( "myNumber menor que 10");
  }