console.log( void( 0 ) );
var object = { one: 1, two: 2 };
delete object.one;  //retorna verdadero
document.write(void( delete object.one));
document.write("<br>" +object.two );
document.write("<br>" +void( object.two ));
console.log( void( delete object.one));
console.log( object.two );
console.log( void( object.two ) );

function addNumbers(a, b) {
    return a+b
}


console.log( addNumbers(1, 2) ); // returns 3
console.log( void( addNumbers(1, 2) ) ); // returns Undefine

document.write( "<br>" +addNumbers(1, 2));
document.write("<br>" +void( addNumbers(1, 2) ));