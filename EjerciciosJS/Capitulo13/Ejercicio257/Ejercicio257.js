function *myGen(){
    yield 'generator function'
}
var iterator = myGen();
console.log(iterator.next());
console.log(iterator.next());
var GeneratorFunction = Object.getPrototypeOf(function*(){}).constructor;
var myGenFunction = new GeneratorFunction('value', 'yield value');
var myGenIterator = myGenFunction();
console.log(myGenIterator.next());
console.log(myGenIterator.next());
