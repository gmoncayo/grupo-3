/*¿Cómo llamar a los métodos del padre de un objeto?
Problema
Desea utilizar un método que no esté definido en el objeto actual sino en el objeto primario del objeto*/

class Ship{
  constructor(name, type, color){
  this.name = name;
  this.type = type;
  this.color = color;
  }

  shipName(){
    return 'I am ' + this.name;
    }
    shipType(){
    return 'I am type: ' + this.type;
    }
    shipColor(){
    return 'My color is ' + this.color;
    }
}
  class SpaceShip extends Ship{
    constructor(type, name, color){
    super(type, name, color)
    }
    spaceShipName(){
    return super.shipName();
    }
    spaceShipType(){
    return super.shipType();
    }
    spaceShipColor(){
    return super.shipColor();
    }
  }