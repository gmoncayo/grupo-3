import React from "react";

const Login = (props) => {

    const {
        email, 
        setEmail, 
        password, 
        setPassword, 
        handleLogin, 
        handleSigup, 
        hasAccount, 
        setHasAccount, 
        emailError, 
        passwordError,
    } = props;

     //linkObject son los datos que quiero guardar
     const [links, setLinks] = useState([])

     const addOrEditLink = async (linkObject) => {
         //console.log(linkObject)
         //evento asincrono
         await db.collection('links').doc().set(linkObject)
         console.log('New task added')
 
     }
 
     const getLinks = async () => {
        db.collection('links').onSnapshot((querySnapshot) => {
             const docs = [];
             querySnapshot.forEach((doc) => {
                 docs.push({...doc.data(), id:doc.id})
             });
             setLinks(docs);
         });
     }
 
     useEffect(() => {
         getLinks();
     }, [])
/*
    //metodo para validar mails duplicados
    function unicidadMail() {
        var mail= document.getElementById("IMail);
        var mailUsuario = new Date(mail.value);
        links.map(link => {      
            console.log(link.email)
        });
    }
¨*/

    return(
        <section className="login">
            <div className="loginContainer">
                <label>Nombre de usuario</label>
                <input
                    type="text"
                    id="IMail"
                    autoFocus
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <p className="errorMsg">{emailError}</p>
                <label>Contraseña</label>
                <input 
                    type="password" 
                    required 
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                />
                <p className="errorMsg">{passwordError}</p>
                <div className="btnContainer">
                    {hasAccount ? (
                        <>
                            <button onClick={handleLogin}>Iniciar sesión</button>
                            <p>
                                ¿ No tiene una cuenta ? 
                                <span onClick={() => setHasAccount(!hasAccount)}>Registrarse</span>
                            </p>
                        </>                            
                    ) : (
                        <>
                            <button onClick={handleSigup}>Registrarse</button>                        
                            <p>
                                ¿ Tiene una cuenta ? 
                                <span onClick={() => setHasAccount(!hasAccount)}>Iniciar sesión</span></p>
                        </>
                    ) }
                </div>
            </div>
        </section>
    );
    };

export default Login;