import React, { useState, useEffect } from "react";
import firebase from 'firebase';  
import Login from './Login';
import Hero from "./components/Hero";
import "./App.css";

const App = () => {
  const [user, setUser] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");  
  const [passwordError, setPasswordError] = useState("");  
  const [hasAccount, setHasAccount] = useState(false);  

  const clearInputs =() => {
    setEmail('');
    setPassword('');
  }

  const clearErrors = () => {
    setEmailError('');
    setPasswordError('');
  } 



  //firebase me permite crear una autentificacion y validacion llamando a las funciones predeterminadas auth/wrong-password
  // el cual valida si la contrase�a es superoir a 6 caracteres
  //sin embargo para realizar logins y validaciones customizables, se debe crear de cero un sistema de autentificacion personalizado
  //usando las claves del servidor del proyecto, que ene ste caso posee Carlos, y en el limitado tiempo es complicado de implementar 
  // se debe recibir el token personalizado de tu servidor de autenticaci�n, p�salo a signInWithCustomToken para que el usuario acceda
/*
  firebase.auth().signInWithCustomToken(token).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // ...
  });

  */


  const handleLogin = () => {
    clearErrors();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch((err) => {
        switch (err.code){
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":  
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
        }
      });
  };
 
  const handleSigup = () => {
    clearErrors();
    firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .catch((err) => {
      switch (err.code){
        case "auth/email-already-in-use":
        case "auth/invalid-email":
          setEmailError(err.message);
          break;
        case "auth/weak-password":
          setPasswordError(err.message);
          break;
      }
    });
  };
  
const handleLogout = () => {
  firebase.auth().signOut();
};

const authListener = () => {
  firebase.auth().onAuthStateChanged((user) => {
    if(user) {
      clearInputs();
      setUser(user);
    } else {
      setUser("");
    }
  });
};

useEffect(() => {
  authListener();
}, []);

  return (
    <div className="App">
      {user ? (
        <Hero handleLogout={handleLogout} />        
      ) : (
        <Login 
        email={email} 
        setEmail={setEmail} 
        password={password} 
        setPassword={setPassword} 
        handleLogin={handleLogin}
        handleSigup={handleSigup}
        hasAccount={hasAccount}
        setHasAccount={setHasAccount}
        emailError={emailError}
        passwordError={passwordError}
         />
      )}
      
    </div>
  );
};

export default App;